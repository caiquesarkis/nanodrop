# Nanodrop

> Nanodrop is a molecular dynamics based on the Lennard Jones potential. 

## Features 
- Phase Transition
    - Solid
    - Liquid
    - Gás
- Termometer (The unit is a measure of knetic energy)
- Colored particles (represents their potential energy).

**Simulation Site :**   https://caiquesarkis.gitlab.io/nanodrop/

# About me
Author: Caique Sarkis ([@caique_sarkis](https://twitter.com/caique_sarkis))
