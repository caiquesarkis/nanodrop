// A solution for import csv :
//https://www.therogerlab.com/how-can-i/javascript/read-a-csv.html#readCSV

// A solution for export csv :
// https://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side


function processFile(){
    var file = document.querySelector('#myFile').files[0];
    var reader = new FileReader();
    reader.readAsText(file);
    
  
    //if you need to read a csv file with a 'ISO-8859-1' encoding
    /*reader.readAsText(file,'ISO-8859-1');*/
  
    //When the file finish load
    reader.onload = function(event) {
  
      //get the file.
      var csv = event.target.result;
  
      //split and get the rows in an array
      var rows = csv.split('\n');

      csvPlacement(rows); 
      paintCanvas();								// draw initial state
      simulate();		// initializations are finished so let 'er rip!
      // Those functions are placed here to solve a sync problem
      
    }   
  } 


