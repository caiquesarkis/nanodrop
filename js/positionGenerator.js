// Generates the initial position of the particles using 
// polar coordinates


function circularPlacement (){
    let particleRadius = 1.5;
    let phi= 0;
    let r = particleRadius + 1.5;
    for (let i = 0; i<N; i++){

        x[i] = r*Math.cos(phi) + canvas.width*0.05;
        y[i] = r*Math.sin(phi) + canvas.height*0.05;
        
        phi += 0.3 * 2 * Math.PI/r

        if (phi >= 2*Math.PI - .1){
            phi = 0;
            r += particleRadius
        }
    }
}

function gridPlacement (){
    // Place molecules in rows:
    var neighborSpace = 1.5;					// initial distance between molecules
    var nextX = neighborSpace;
    var nextY = neighborSpace;
    for (var i=0; i<N; i++) {
        x[i] = nextX; y[i] = nextY;

        nextX += neighborSpace;
        if (nextX > boxWidth - 1) {
            nextX = neighborSpace;
            nextY += neighborSpace;
        }
    }
}


function csvPlacement (array){
    N = array.length -1
    arrayInitalization(N);
    for (var i = 1; i < array.length; i++) {
        //split by separator (,) and get the columns
        cols = array[i].split(',');
        
        x[i-1] = parseFloat(cols[0]);
        y[i-1] = parseFloat(cols[1]);
      }


}



