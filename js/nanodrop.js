// Global variables:	
var mobile = navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|Opera Mini|IEMobile/i)
var N = 1;								// number of molecules
var dt = 0.005;								// time step in natural units
var wallStiffness = 50;						// spring constant for bouncing off walls
var forceCutoff = 3.0;						// distance beyond which we set force=0
var forceCutoff2 = forceCutoff*forceCutoff;
var canvas = document.getElementById('theCanvas');
var context = canvas.getContext('2d');
var pxPerUnit = 10;							// molecule diameter in pixels
//if (mobile) pxPerUnit = 20;
var boxWidth = canvas.width / pxPerUnit;	// width of box in natural units
var running = false;						// will be true when running
var pauseButton = document.getElementById('pauseButton');
//var gravSlider = document.getElementById('gravSlider');
var tempSlider = document.getElementById('tempSlider');
//var speedReadout = document.getElementById('speedReadout');
var confiningButton = document.getElementById('confiningButton');
var stepsPerFrame = 25;
var stepCount = 0;
var startTime = 0;
var loaded = false;
var Exporting = false;
var Export = [];
var simulationTime = 100;
var confiningPotentialRunning = true;
let initialized = false;


// Mysterious incantation that sometimes helps for smooth animation:
window.requestAnimFrame = (function(callback) {
	return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1);		// second parameter is time in ms
		};
	})();



function arrayInitalization(N){
		
	// Create the arrays of molecule positions, velocities, and accelerations:
	globalThis.x = new Array(N), globalThis.y = new Array(N);
	globalThis.vx = new Array(N), globalThis.vy = new Array(N);
	globalThis.ax = new Array(N), globalThis.ay = new Array(N);

	globalThis.PE = new Array(N);

	// Initial arrangement of particles

	for (let i = 0; i<N; i++){
		//vx[i] = Math.random(); vy[i] = Math.random();
		vx[i] =0.0; vy[i] = 0.0;
		ax[i] = 0.0; ay[i] = 0.0;
	}
}



function initialize (){
	// Escolha de formato inicial
	//var e = document.getElementById("init");
	var option = "circular";
	N =  document.getElementById("N").value

	if (initialized == false){
		//var ex = document.getElementById("export");
		if (option == "circular"){
			arrayInitalization(N);
			circularPlacement();
			paintCanvas();								// draw initial state
			simulate();		// initializations are finished so let 'er rip!
			// Makes the simulation start imediatly after parameter initialization
			startStop()
		}else if  (option == "grid"){
			arrayInitalization(N);
			gridPlacement();
			paintCanvas();								// draw initial state
			simulate();		// initializations are finished so let 'er rip!
			// Makes the simulation start imediatly after parameter initialization
			startStop()
		}
		initialized = true;
	}
	

	/*
	else if  ((option == "csv") && (!loaded)){


		document.getElementById("csvContainer").innerHTML += "<input type='file' id='myFile'>"
		document.getElementById("csvContainer").innerHTML += "<button onclick='load()'2 id='myButton'>Load</button>"
		// Make a way to have the number N initialized by the csv file
		loaded = true;
		// Makes the simulation start imediatly after parameter initialization
		startStop()
	}
	option = e.value;
	if (option == "yes"){
		Exporting = true;
	}
 	*/
	
	
}

	
function load(){
	processFile();
	// make this getElemenst a single variable at the top of the program
	// it will probably make it faster
	document.getElementById("myFile").remove()
	document.getElementById("myButton").remove()
	loaded = false;
}

function exportCsv(){

    const rows = Export

    var csv = 'x,y\n';
    rows.forEach(function(row) {
            csv += row.join(',');
            csv += "\n";
    });

    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'simulation.csv';
	hiddenElement.click();
	Exporting = false;
  }

	


	

// Simulate function executes a bunch of steps and then schedules another call to itself:
function simulate() {
	var aux = stepCount/stepsPerFrame
	if (running) {
		// Execute a bunch of time steps:
		for (var step=0; step<stepsPerFrame; step++) {
			doStep();
		}

		if (aux%20 == 0){
			velMeasure()
		}
		
		targetTemperature()

		// Populate the csv file
		if (Exporting){
			for (let i =0; i<N;i++){
				Export.push([x[i],y[i]]);
			}
			
			Export.push(["Step Count: ",aux])
			if (aux >= simulationTime){
				exportCsv();
			}		
		}

		paintCanvas();	
		stepCount += stepsPerFrame;
		var elapsedTime = ((new Date()).getTime() - startTime) / 1000;	// time in seconds
		//speedReadout.innerHTML = Number(stepCount/elapsedTime).toFixed(0);
	}
	// schedule the next animation frame:
	//requestAnimFrame(function() { simulate(); });		// smoother on Safari
	window.setTimeout(simulate, 1);					// faster; smoother on Firefox
}




//							Dynamics



function velMeasure(){
	var aux = 0;
	for (let i=0;i < N ; i++){
		aux += 0.5 * (vx[i]*vx[i] + vy[i]*vy[i]);
	}
	aux = aux/N
	
	temperatureReadout.innerHTML = Number(aux).toFixed(1)
}

function confiningPotential(){
	//Add confining potential
	
	if (confiningPotentialRunning){
		for (var i = 0; i < N; i++) {
			ax[i] += 0.01* (0.5 * boxWidth - x[i]);
			ay[i] += 0.01* (0.5 * boxWidth - y[i]);
		}
		//confiningButton.value = "Confinement: On";

		
		
		
	} else {
		//confiningButton.value = "Confinement: Off";
	}
}
function confiningPotentialToggle(){
	confiningPotentialRunning = !confiningPotentialRunning
}


// Execute a single time step (Verlet algorithm):
function doStep() {
	for (var i=0; i<N; i++) {
		x[i] += vx[i]*dt + 0.5*ax[i]*dt*dt;
		y[i] += vy[i]*dt + 0.5*ay[i]*dt*dt;
		vx[i] += 0.5*ax[i]*dt;
		vy[i] += 0.5*ay[i]*dt;
	}
	//confiningPotential()
	computeAccelerations();

	for (var i=0; i<N; i++) {
		vx[i] += 0.5*ax[i]*dt;
		vy[i] += 0.5*ay[i]*dt;
	}
}


// Compute accelerations of all molecules:
function computeAccelerations() {
	var dx, dy, dx2, dy2, rSquared, rSquaredInv, attract, repel, fOverR, fx, fy, tempPE 	;
	//var g = Number(gravSlider.value);
	// first check for bounces off walls:
	for (var i = 0; i < N; i++) {
		if (x[i] < 0.5) {
			ax[i] = wallStiffness * (0.5 - x[i]);
		} else
			if (x[i] > (boxWidth - 0.5)) {
				ax[i] = wallStiffness * (boxWidth - 0.5 - x[i]);
			} else
				ax[i] = 0.0;
		if (y[i] < 0.5) {
			ay[i] = (wallStiffness * (0.5 - y[i]));
		} else
			if (y[i] > (boxWidth - 0.5)) {
				ay[i] = (wallStiffness * (boxWidth - 0.5 - y[i]));
			} else
				ay[i] = 0;
		//ay[i] -= g;				// add gravity if any
	}

	confiningPotential();
	

	//pressure = wallForce / (4*boxWidth);
	// now compute interaction forces (Lennard-Jones potential):
	for (var i = 0; i < N; i++) {
		tempPE = 0;
		for (var j = 0; j < N; j++) {
			dx = x[i] - x[j];
			dx2 = dx * dx;
			if ((dx2 < forceCutoff2) && (i != j)) {  // make sure they're close enough to bother
				dy = y[i] - y[j];
				dy2 = dy * dy;
				if (dy2 < forceCutoff2) {
					rSquared = dx2 + dy2;
					if (rSquared < forceCutoff2) {
						rSquaredInv = 1.0 / rSquared;
						attract = rSquaredInv * rSquaredInv * rSquaredInv;
						repel = attract * attract;
						//tempPE += (4.0 * (repel - attract)) - pEatCutoff;
						tempPE += (4.0 * (repel - attract));
						
						fOverR = 24.0 * ((2.0 * repel) - attract) * rSquaredInv;
						
						fx = fOverR * dx;
						fy = fOverR * dy;
						ax[i] += fx;  // add this force on to i's acceleration (m = 1)
						ay[i] += fy;
						ax[j] -= fx;  // Newton's 3rd law
						ay[j] -= fy;
					}
				}
			}
		}
		
		
		PE[i] = tempPE;
		

	}
}

// 							Visuals



// Create a color map based on a array of numbers
function var2Hsl(array){
	let max =  Math.max(...array)
	let min =  Math.min(...array)
	let colorMap = []
	for (let i = 0; i<N ;i++){
		colorMap[i] = `hsl(${100 - 100*(array[i] - min)/(max-min)}, 97%, 50%)`
	}
	return colorMap
}

// Clear the canvas and draw all the molecules:
function paintCanvas() {
	context.fillStyle = '#fff';		// black
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = '#00ff00';		// green
	
	let colorMap = var2Hsl(PE)
	for (var i=0; i<N; i++) {
		var pixelX = x[i] * pxPerUnit;
		var pixelY = canvas.height - (y[i] * pxPerUnit);
		context.beginPath();
		context.arc(pixelX, pixelY, pxPerUnit/2, 0, 2*Math.PI);
		
		context.fillStyle = colorMap[i]
		//context.fillStyle = `rgb(${Math.floor(255*(PE[i] - min) / max)},${Math.floor(0)},${Math.floor(0)})`;
		context.fill();
	}
}


// 						Functionality



function targetTemperature(){
	var t = Number(tempSlider.value);
	var gap = parseFloat(temperatureReadout.innerHTML) - t
	if (gap  > 0){
		if (Math.abs(gap) > 0.3){
			for (var i = 0; i < N; i++) {
				vx[i] *= 0.99;
				vy[i] *= 0.99;
			}
		}
		
		
	}
	if (gap < 0) {
		if (Math.abs(gap) > 0.3){
			for (var i = 0; i < N; i++) {
				vx[i] *= 1.01;
				vy[i] *= 1.01;
			}
		}
	}
}




// Function to start or pause the simulation:
function startStop() {
	running = !running;
	if (running) {
		pauseButton.value = " Pause ";
		stepCount = 0;
		startTime = (new Date()).getTime();
	} else {
		pauseButton.value = "Resume";
	}
}

// Function to change all speeds by a given factor (called by button presses):
function speedFactor(factor) {
	for (var i=0; i<N; i++) {
		vx[i] *= factor;
		vy[i] *= factor;
	}
}


